# Metasys Connector
Author: [Xiaohan Fu](xhfu.me)

## File Structure
`actuat*` are files for the actuation service. a grpc endpoint is listening to actuation queries.

`reader.py` is the data streamer utilizing the METASYS subscription feature

`get_token.py` is the master process that spawns subprocesses for the above two services and keeps the metasys token used by those processes fresh with shared memory.

`config.json` specifies metasys hostname, credential, kafka/mqtt params, and grpc listening address desired as well as the target network device to collect data from.


## Get Started
Prepare a virtual env (recommended). Install dependencies with PDM `pdm sync --clean`.

All services (token maintainer, actuater and data streamer) will be spawned with `python get_token.py`. It's recommended to set it up as a system service with `systemctl` to make sure it will be auto restarted on system poweroff etc. In the rest of the cases, the program should be able to correct itself on failure without human interventions.

Depending on the status of ADX server and the number of network device to subscribe, the start-up would take up to a few minutes. Check `./logs` for status. 
