from concurrent import futures
from util import create_logger
import grpc
import actuate_pb2
import actuate_pb2_grpc
import os
import httpx

# token = {
#     'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlJsMUYtQUlPMEY5bDlsRENEaVQ3R3lqLW5vcyIsImtpZCI6IlJsMUYtQUlPMEY5bDlsRENEaVQ3R3lqLW5vcyJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0Ojk1MDYvQVBJLkF1dGhlbnRpY2F0aW9uU2VydmljZSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTUwNi9BUEkuQXV0aGVudGljYXRpb25TZXJ2aWNlL3Jlc291cmNlcyIsImV4cCI6MTY3OTQ0OTA2MCwibmJmIjoxNjc5NDQ3MjYwLCJjbGllbnRfaWQiOiJtZXRhc3lzX2FwaSIsInNjb3BlIjpbIm1ldGFzeXNfYXBpIiwib2ZmbGluZV9hY2Nlc3MiLCJvcGVuaWQiXSwic3ViIjoiMDE1NThlM2ItNTg5ZS00ZjI0LWFlMjAtZWZjYTE5MmFhZWM5IiwiYXV0aF90aW1lIjoxNjc5NDQ3MjYwLCJpZHAiOiJpZHNydiIsIlVzZXJJZCI6IjcyMTkiLCJVc2VyTmFtZSI6InhpYW9oYW5hcGkiLCJJc0FkbWluIjoiRmFsc2UiLCJJc1Bhc3N3b3JkQ2hhbmdlUmVxdWlyZWQiOiJGYWxzZSIsIklzVGVybXNBbmRDb25kaXRpb25zUmVxdWlyZWQiOiJGYWxzZSIsIkN1bHR1cmUiOiJlbi1VUyIsIkFjdGl2ZUxpY2Vuc2VzIjoiW3tcImZlYXR1cmVcIjp7XCJuYW1lXCI6XCJBRFhcIixcInZlcnNpb25cIjpcIjEyLnhcIn0sXCJzdGF0dXNcIjoxLFwiZXhwaXJ5RGF0ZVwiOlwiOTk5OS0xMi0zMVQyMzo1OTo1OS45OTk5OTk5XCIsXCJkYXlzVG9FeHBpcmVcIjoyOTEzNDk1LFwiVHlwZVwiOjV9LHtcImZlYXR1cmVcIjp7XCJuYW1lXCI6XCJNT05DTUQtQVBJXCIsXCJ2ZXJzaW9uXCI6XCIxMi54XCJ9LFwic3RhdHVzXCI6MSxcImV4cGlyeURhdGVcIjpcIjk5OTktMTItMzFUMjM6NTk6NTkuOTk5OTk5OVwiLFwiZGF5c1RvRXhwaXJlXCI6MjkxMzQ5NSxcIlR5cGVcIjo1fSx7XCJmZWF0dXJlXCI6e1wibmFtZVwiOlwiQURGU1wiLFwidmVyc2lvblwiOlwiMTIueFwifSxcInN0YXR1c1wiOjIsXCJleHBpcnlEYXRlXCI6XCIwMDAxLTAxLTAxVDAwOjAwOjAwXCIsXCJkYXlzVG9FeHBpcmVcIjowLFwiVHlwZVwiOjB9LHtcImZlYXR1cmVcIjp7XCJuYW1lXCI6XCJGQVVMVFwiLFwidmVyc2lvblwiOlwiMTEueFwifSxcInN0YXR1c1wiOjEsXCJleHBpcnlEYXRlXCI6XCI5OTk5LTEyLTMxVDIzOjU5OjU5Ljk5OTk5OTlcIixcImRheXNUb0V4cGlyZVwiOjI5MTM0OTUsXCJUeXBlXCI6NX0se1wiZmVhdHVyZVwiOntcIm5hbWVcIjpcIlRSSUFHRVwiLFwidmVyc2lvblwiOlwiMTEueFwifSxcInN0YXR1c1wiOjEsXCJleHBpcnlEYXRlXCI6XCI5OTk5LTEyLTMxVDIzOjU5OjU5Ljk5OTk5OTlcIixcImRheXNUb0V4cGlyZVwiOjI5MTM0OTUsXCJUeXBlXCI6NX0se1wiZmVhdHVyZVwiOntcIm5hbWVcIjpcIlNNUFwiLFwidmVyc2lvblwiOlwiMTIueFwifSxcInN0YXR1c1wiOjEsXCJleHBpcnlEYXRlXCI6XCI5OTk5LTEyLTMxVDIzOjU5OjU5Ljk5OTk5OTlcIixcImRheXNUb0V4cGlyZVwiOjI5MTM0OTUsXCJUeXBlXCI6NX0se1wiZmVhdHVyZVwiOntcIm5hbWVcIjpcIkJBQ05FVC9TQ1wiLFwidmVyc2lvblwiOlwieC54XCJ9LFwic3RhdHVzXCI6MixcImV4cGlyeURhdGVcIjpcIjAwMDEtMDEtMDFUMDA6MDA6MDBcIixcImRheXNUb0V4cGlyZVwiOjAsXCJUeXBlXCI6MH0se1wiZmVhdHVyZVwiOntcIm5hbWVcIjpcIk9BUy1ISVNUXCIsXCJ2ZXJzaW9uXCI6XCJ4LnhcIn0sXCJzdGF0dXNcIjoxLFwiZXhwaXJ5RGF0ZVwiOlwiOTk5OS0xMi0zMVQyMzo1OTo1OS45OTk5OTk5XCIsXCJkYXlzVG9FeHBpcmVcIjoyOTEzNDk1LFwiVHlwZVwiOjV9LHtcImZlYXR1cmVcIjp7XCJuYW1lXCI6XCJPQVMtUFBBXCIsXCJ2ZXJzaW9uXCI6XCJ4LnhcIn0sXCJzdGF0dXNcIjoxLFwiZXhwaXJ5RGF0ZVwiOlwiOTk5OS0xMi0zMVQyMzo1OTo1OS45OTk5OTk5XCIsXCJkYXlzVG9FeHBpcmVcIjoyOTEzNDk1LFwiVHlwZVwiOjV9LHtcImZlYXR1cmVcIjp7XCJuYW1lXCI6XCJPQVMtU0NIRURSUFRcIixcInZlcnNpb25cIjpcIngueFwifSxcInN0YXR1c1wiOjEsXCJleHBpcnlEYXRlXCI6XCI5OTk5LTEyLTMxVDIzOjU5OjU5Ljk5OTk5OTlcIixcImRheXNUb0V4cGlyZVwiOjI5MTM0OTUsXCJUeXBlXCI6NX1dIiwiYW1yIjpbInBhc3N3b3JkIl19.L3ezPnUugFIZQL4TLB3mAeO_Z9Z_K-mhbgZtNyEDu9QGbEY67F64SZgvucGjowDh3y1AYYdEuqd4RSFZB9LBzYlG_2lsQNoSXAhLUuY5y6kDV4ocbdn40iuDbNNmQn-HbzOWsMWUHJ94VC2zXriWT1kPJY-t3E20kKKTPfF61chvn_4JVDKZcKovlx6uozzHR0EWYK0ASXIv9aQ0UXvLPNEZ8tyRNMQky4ziMYoBYn0vFN6TKwLK1PLL8soE63TDl69navvfAt_a4UbVGiUhUgbkXAuttvs7JwltZo5_DjakNUS-0M3XfsyS_goMlBkc-jK3TFehc2YeoxaYwvhlTw'
# }


class Actuator(actuate_pb2_grpc.ActuateServicer):

    def __init__(self, client_kwargs: dict, shared_token, logger):
        self.client_kwargs = client_kwargs
        self.shared_token = shared_token
        self.logger = logger


    def TemporaryOverride(
            self, request: actuate_pb2.TemporaryOverrideAction,
            context: grpc.ServicerContext) -> actuate_pb2.Response:
        payload = {
            "parameters": [request.value, request.hour if request.hour else 0, request.minute if request.minute else 2],
        }
        if request.annotation: payload['annotation'] = request.annotation

        with httpx.Client(**self.client_kwargs) as client:
            client.headers.update({"Authorization": f"Bearer {self.shared_token['token']}"})
            self.logger.info(self.shared_token['token'])
            try:
                response = client.put(f'/objects/{request.uuid}/commands/temporaryOverrideCommand', json=payload)
                response.raise_for_status()
            except httpx.HTTPStatusError as exc:
                self.logger.error(f"Error response {exc.response.status_code}: {exc.response.content} while requesting {exc.request.url!r} for temporaryOverride request\n{request}")
                return actuate_pb2.Response(status=False, details=response.text)
            except Exception as exc:
                self.logger.error(f'Error {exc} occurred when handling temperorayOverride for request\n{request}')
                return actuate_pb2.Response(status=False, details=f'Error {exc} occurred when handling temperorayOverride request\n{request}')
            else:
                self.logger.info(f'Successfully actuated temporaryOverride request\n{request}')
                return actuate_pb2.Response(status=True, details=response.text)


    def ReadObjectCurrent(self, request: actuate_pb2.ReadObjectCurrentAction, context: grpc.ServicerContext) -> actuate_pb2.Response:
        with httpx.Client(**self.client_kwargs) as client:
            client.headers.update({"Authorization": f"Bearer {self.shared_token['token']}"})
            self.logger.info(self.shared_token['token'])
            try:
                response = client.get(f'/objects/{request.uuid}/attributes/{request.attribute}')
                response.raise_for_status()
            except httpx.HTTPStatusError as exc:
                self.logger.error(f"Error response {exc.response.status_code}: {exc.response.content} while requesting {exc.request.url!r} for readObjectCurrent request\n{request}")
                return actuate_pb2.Response(status=False, details=response.text)
            except Exception as exc:
                self.logger.error(f'Error {exc} occurred when handling readObjectCurrent for request\n{request}')
                return actuate_pb2.Response(status=False, details=f'Error {exc} occurred when handling readObjectCurrent request\n{request}')
            else:
                result = response.json()['item']['presentValue']
                self.logger.info(f'Successfully actuated readObjectCurrent request\n{request} with result {result}')
                return actuate_pb2.Response(status=True, details=str(result))


def serve(client_kwargs: dict, shared_token, listen_addr: str='[::]:50051') -> None:
    logger = create_logger('./logs/actuator.log')
    logger.info(f'Actuator spawned at PID={os.getpid()}')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    actuator = Actuator(client_kwargs, shared_token, logger)
    actuate_pb2_grpc.add_ActuateServicer_to_server(actuator, server)
    server.add_insecure_port(listen_addr)
    server.start()
    logger.info("Server started on %s", listen_addr)
    server.wait_for_termination()


# if __name__ == '__main__':
#     client_kwargs = {
#         'timeout': 20,
#         'verify': False,
#         'base_url': "https://172.21.59.228/api/v4",
#         'headers': {"Authorization": f"Bearer {token['token']}"}
#     }
#     serve(client_kwargs, token)