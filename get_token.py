from time import sleep
import httpx
import arrow
from tenacity import *
from typing import Tuple
from arrow import Arrow
from multiprocessing import Process, Manager
from reader import stream
from actuator import serve
from util import create_logger
import json

logger = create_logger('./logs/token_maintainer.log')

client_kwargs = {
    'timeout': 60,
    'verify': False,
}


@retry(wait=wait_exponential(multiplier=1, min=1, max=10))
def init_token(client: httpx.Client, username: str, password: str) -> Tuple[str, Arrow]:
    try:
        logger.info('attempt to login')
        response = client.post(f"/login", json={"username": username, "password": password})
        response.raise_for_status()
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc
    except httpx.RequestError as exc:
        logger.error(f"An error {exc} occurred while requesting {exc.request.url!r}.")
        raise exc
    token = response.json()['accessToken']
    expiration = arrow.get(response.json()['expires'])
    logger.info(f"Token obtained. Token\n{token}\nexpires at {expiration}")
    return token, expiration


@retry(wait=wait_exponential_jitter(initial=5, max=60, jitter=5), stop=stop_after_attempt(20)) # retry(wait=wait_exponential(multiplier=1, min=1, max=10))
def refresh_token(client: httpx.Client) -> Tuple[str, Arrow]:
    try:
        response = client.get(f"/refreshToken")
        response.raise_for_status()
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc # when this is raised, no retry and exit immediately since this implies an api endpoint error or other unexpected circumstances
    except httpx.RequestError as exc:
        logger.error(f"An error [{exc}] occurred while requesting {exc.request.url!r}.")
        raise exc
    token = response.json()['accessToken']
    expiration = arrow.get(response.json()['expires'])
    logger.info(f"Token refreshed. New token\n{token}\nexpires at {expiration}")
    return token, expiration


def refresh_token_forever(client_kwargs, shared_token, expiration):
    with httpx.Client(**client_kwargs) as client:
        while(arrow.utcnow() < expiration):
            logger.info(f'token maintainer sleeping now... {300}s')
            sleep(300)
            token, expiration = refresh_token(client)
            shared_token['token'] = token
            client.headers.update({"Authorization": f"Bearer {token}"})


def main():
    while True:
        logger.info("Master token manager initiation")
        with open("config.json") as f, Manager() as manager:
            config = json.load(f)
            username = config["username"]
            password = config["password"]
            hostname = config["hostname"]
            client_kwargs['base_url'] = f"https://{hostname}/api/v4"
            with httpx.Client(**client_kwargs) as client:
                token, expiration = init_token(client, username, password)
            client_kwargs['headers'] = {"Authorization": f"Bearer {token}"}

            shared_token = manager.dict({'token': token})
            token_maintainer = Process(target=refresh_token_forever, args=(client_kwargs, shared_token, expiration))
            target_network_devices = config['target_network_devices']
            streamer = Process(target=stream, args=(client_kwargs, target_network_devices, shared_token, config['mqtt_params']))
            actuator = Process(target=serve, args=(client_kwargs, shared_token, config['grpc_addr']))
            
            token_maintainer.start()
            logger.info(f'Token maintainer spawned with pid {token_maintainer.pid}')
            streamer.start()
            logger.info(f'Streamer spawned with pid {streamer.pid}')
            actuator.start()
            logger.info(f'Actuator spawned with pid {actuator.pid}')

            token_maintainer.join() # token manager is dead, which means token has expired or metasys has failed
            logger.error("token expired or metasys host failed. restarting all services")
            if streamer.is_alive(): streamer.terminate()
            if actuator.is_alive(): actuator.terminate()
            

if __name__ == "__main__":
    main()