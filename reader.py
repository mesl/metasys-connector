import time, traceback, re
from typing import Iterable
import httpx
from httpx_sse import SSEError, connect_sse
# from confluent_kafka import Producer
import socket, json, os
from util import create_logger, UUID_PATTERN, UUID_FAKE
from datetime import datetime

import logging
from tenacity import *
# from influxdb_client import WritePrecision, InfluxDBClient, Point, WriteOptions
# from influxdb_client.client.write_api import SYNCHRONOUS
import paho.mqtt.client as mqtt

logger = create_logger('./logs/reader.log', logging.INFO)

# token = 'y6CgzC9W3smC-RlBPFenYtByZz_kMVxcZ2-KwYc5ZJN08HuyZx52n5lVfKUA2RDBu1z55_M7Oav9UxHcWCR6fw=='
# org = '9d4d3af8fd50fcbb'
# bucket = 'CO2-Exp'
# url = 'https://us-east-1-1.aws.cloud2.influxdata.com'
# db_client = InfluxDBClient(url=url, token=token, org=org)

# db_write_client = db_client.write_api(write_options=WriteOptions(batch_size=100,
#                                                       flush_interval=10_000,
#                                                       jitter_interval=2_000,
#                                                       retry_interval=5_000,
#                                                       max_retries=5,
#                                                       max_retry_delay=30_000,
#                                                       max_close_wait=300_000,
#                                                       exponential_base=2))       
# db_write_client = db_client.write_api(write_options=SYNCHRONOUS)

@retry(wait=wait_exponential_jitter(initial=1, max=60, jitter=5), after=after_log(logger, logging.ERROR), stop=stop_after_attempt(10))
def get_equipment(client: httpx.Client, network_device: str) -> set[str]:
    '''
    network_device: id of the desired network device
    return a list of ids of equipment associated with the specified network device
    '''
    try:
        response = client.get(f"/networkDevices/{network_device}/equipment")
        response.raise_for_status()

        equipment_set = {equipment['id'] for equipment in response.json()['items']}
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc
    except httpx.RequestError as exc:
        logger.error(f"An error {exc} occurred while requesting {exc.request.url!r}.")
        raise exc
    except Exception as exc:
        logger.error(exc)
        raise exc
    return equipment_set


@retry(wait=wait_exponential_jitter(initial=1, max=60, jitter=5), after=after_log(logger, logging.ERROR), stop=stop_after_attempt(10))
def get_points(client: httpx.Client, equipment: str) -> set[str]:
    '''
    equipment: id of the desired equipment
    return a list of *object* ids of the points within this equipment
    '''
    try:
        response = client.get(f"/equipment/{equipment}/points")
        response.raise_for_status()
        
        points = {re.search(UUID_PATTERN, point['objectUrl']).group(0) for point in response.json()['items']}
        if UUID_FAKE in points:
            points.remove(UUID_FAKE)
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc
    except httpx.RequestError as exc:
        logger.error(f"An error {exc} occurred while requesting {exc.request.url!r}.")
        raise exc
    except Exception as exc:
        logger.error(exc)
        raise exc

    return points


@retry(wait=wait_exponential_jitter(initial=1, max=60, jitter=5), after=after_log(logger, logging.ERROR), stop=stop_after_attempt(10))
def get_network_device_by_name(client: httpx.Client, network_device_name: str) -> str:
    '''
    network_device_name: device name (keyword) of the desired network device e.g. 'SNE-198'
    return the id of the network device with this keyword in its name
    '''
    try:
        response = client.get(f"/networkDevices", params={'pageSize': '1000', 'classification': 'device'})
        response.raise_for_status()
        print(response.elapsed.total_seconds())
        network_device: set[str] = {device['id'] for device in response.json()['items'] if device['itemReference'].find(network_device_name) != -1}
        if len(network_device) > 1:
            raise ValueError('The device name keyword provided does not lead to a unique result. Try things like "SNE-198" rather than building names.')
        elif len(network_device) < 1:
            raise ValueError('Not found any device with the name keyword provided.')
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc
    except httpx.RequestError as exc:
        logger.error(f"An error {exc} occurred while requesting {exc.request.url!r}.")
        raise exc
    except Exception as exc:
        logger.error(exc)
        raise exc
        
    return network_device.pop() # return the first one that matches by default


@retry(wait=wait_exponential_jitter(initial=1, max=60, jitter=5), after=after_log(logger, logging.ERROR), stop=stop_after_attempt(10))
def get_network_devices(client: httpx.Client) -> str:
    '''
    call /networkDevices and return the response
    '''
    try:
        response = client.get(f"/networkDevices", params={'pageSize': '1000', 'classification': 'device'})
        response.raise_for_status()
        print(response.elapsed.total_seconds())
    except httpx.HTTPStatusError as exc:
        logger.error(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")
        raise exc
    except httpx.RequestError as exc:
        logger.error(f"An error {exc} occurred while requesting {exc.request.url!r}.")
        raise exc
    except Exception as exc:
        logger.error(exc)
        raise exc
        
    return response.json()


def get_points_by_network_device_names(client: httpx.Client, network_device_names: Iterable[str]) -> set[str]:
    '''
    network_devices: name of target network devices in a list e.g. ['SNE-15', 'SNE-16']
    return a list of *object* ids of the points within the target network devices
    '''
    NE = json.load(open('NE.json','r'))
    points = set()
    for name in network_device_names:
        # network_device = get_network_device_by_name(client, name)
        network_device = NE['FM-ADX:'+name]
        equipment_list = get_equipment(client, network_device)
        for equipment in equipment_list:
            points.update(get_points(client, equipment))
    return points


def get_points_by_equipment_list(client: httpx.Client, equipment_list: Iterable[str]) -> set[str]:
    '''
    equipment_list: a list of equipment id
    return a list of *object* ids of all the points within the target equipment list
    '''
    points = set()
    for equipment in equipment_list:
        points.update(get_points(client, equipment))
    return points


def get_objects_by_network_device(client: httpx.Client, network_device: str) -> set[str]:
    '''
    network_device: id of the target network device
    '''
    response = client.get(f"/networkDevices/{network_device}/objects")
    response.raise_for_status()

    objects = {obj['id'] for obj in response.json()['items'] if 'trend' not in obj['name'].lower() and 'alarm' not in obj['name'].lower()} # trend/alarm objects excluded
    return objects


def get_object_itemReference_by_id(client: httpx.Client, objects: list[str]):
    '''
    objects: list of object ids
    '''
    itemReferences = {}
    for obj in objects:
        try:
            response = client.get(f"/objects/{obj}")
            response.raise_for_status()
        except httpx.HTTPStatusError as exc:
            if exc.response.status_code == 404:
                continue
            raise exc
        itemReferences[obj] = response.json()['item']['itemReference']
        time.sleep(.2)
    return itemReferences


def subscribe_points(client: httpx.Client, stream_id: str, points: list[str]):
    '''
    stream_id: id of the stream event
    points: ids of points to be subscribed
    '''
    payload = {}
    payload["requests"] = [{"id": idx+1, "relativeUrl": f'{point}/attributes/presentValue'} for idx, point in enumerate(points)]
    payload["method"] = "GET"
    client.post("/objects/batch", json=payload, headers={'METASYS-SUBSCRIBE': stream_id})


def get_producer(kafka_host: str, logger):
    conf = {
        'bootstrap.servers': kafka_host,
        'client.id': socket.gethostname(),
        'logger': logger
        }
    producer = Producer(conf)
    return producer


# acked for kafka
def acked(err, msg):
    if err is not None:
        logger.error("Failed to deliver message: %s: %s" % (str(msg), str(err)))
    else:
        logger.debug("Message produced: %s" % (str(msg)))


def prepare_mqtt(mqtt_params):
    mqtt_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1)
    mqtt_client.connect(mqtt_params['broker_addr'], mqtt_params['broker_port'])
    mqtt_client.loop_start()

    return mqtt_client


def stream(client_kwargs: dict, target_network_devices: list[str], shared_token, mqtt_params: dict) -> None:
    logger.info(f'Reader spawned at PID={os.getpid()}')

    # producer = get_producer(kafka_host, logger)
    mqtt_client = prepare_mqtt(mqtt_params)

    count = 0

    with httpx.Client(**client_kwargs) as client:
        last_event_id = ''

        while True: 
            # update the token whenever the stream process restarts. e.g. token expired, other HTTP erros etc.
            client.headers.update({"Authorization": f"Bearer {shared_token['token']}"})

            try:
                points = set()
                points.update(get_points_by_network_device_names(client, target_network_devices))
                # keep this hardcoded line here
                points.update(get_points_by_equipment_list(client, ['e54fb8a6-3d41-53b5-a344-52f25679d5b8', '138c3c0d-11ae-5d7c-bb5e-157aaa1474b2'])) # CSE2150+2109

                assert 'f48b3189-384a-5071-8dec-654e263ff4d7' in points # CENTR222 CO2, a safety check

                logger.info(f"{len(points)} target points obtained")
                logger.info(f"{points}")

                with connect_sse(client, "/stream", headers={"Last-Event-Id": last_event_id} if last_event_id else {}) as event_source: # try reconnect to the existing stream if there's a last_event_id
                    for sse in event_source.iter_sse():
                        if sse.event == "hello":
                            # initialization: subscribe to available points on creating the stream
                            stream_id: str = sse.data[1:-1]
                            logger.info(f"new stream created with id: {stream_id}")
                            subscribe_points(client, stream_id, points)
                            logger.info("succesfully added subscription of the above found points to the stream")
                        elif sse.event == "object.values.update":
                            count += 1
                            dp = json.loads(sse.data)['item']
                            dp['timestamp'] = time.time()
                            print(dp)
                            mqtt_client.publish(mqtt_params['topic'], json.dumps(dp), qos=1, retain=False)
                            # producer.produce(kafka_topic, key=dp['id'], value=json.dumps(dp), callback=acked)
                            # producer.poll(1)
                            # p = Point("heattransfer-test1") \
                            #     .tag("id", dp['id']) \
                            #     .tag("FQR", dp['itemReference']) \
                            #     .field('NumericValue', float(dp['presentValue']) if type(dp['presentValue'])!=str else None) \
                            #     .field('StringValue', dp['presentValue'] if type(dp['presentValue'])==str else None) \
                            #     .time(datetime.utcnow(), WritePrecision.MS)
                            # db_write_client.write(bucket=bucket, record=p)

                        last_event_id = sse.id
            except SSEError as exc:
                if "Stream has expired. Stop reconnecting." in exc.args:
                    logger.error("Previous stream has expired. Create a new stream now.")
                else:
                    logger.error(traceback.format_exc())
                    logger.error("Unexpected SSEError ocurred.")
                last_event_id = ''
                continue
            except Exception as exc:
                logger.error(traceback.format_exc())
                logger.error(f"{exc} occurred. Trying to restart.")
                continue


# testing only, should not be called directly
if __name__ == "__main__":
    with open("config.json") as f:
        config = json.load(f)
        mqtt_client = prepare_mqtt(config["mqtt_params"])
        mqtt_client.disconnect()
