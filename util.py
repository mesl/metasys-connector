import logging
from logging.handlers import TimedRotatingFileHandler

UUID_FAKE = '00000000-0000-0000-0000-000000000000'
UUID_PATTERN = '\w{8}-\w{4}-\w{4}-\w{4}-\w{12}'

def create_logger(logfile, level=logging.INFO):
    logger = logging.getLogger(logfile)
    logger.setLevel(level)
    fh = TimedRotatingFileHandler(logfile, when='midnight',
                                     backupCount=14, encoding=None, delay=0)
    fh.setLevel(level)
    log_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(log_formatter)
    logger.addHandler(fh)
    return logger
